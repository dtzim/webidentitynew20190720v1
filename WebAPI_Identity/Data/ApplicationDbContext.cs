﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using StackForceApi.Models;

namespace WebAPI_Identity.Data
{
    public class ApplicationDbContext : IdentityDbContext<MyUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);


            modelBuilder.Entity<Watchlist>().HasKey(x => new { x.UserId, x.Symbol });


            modelBuilder.Entity<Watchlist>().
            HasOne(w => w.MyUser).
           WithMany(p => p.StocksWatching).
           HasForeignKey(x => x.UserId);

            modelBuilder.Entity<Watchlist>().
           HasOne(w => w.Stock).
           WithMany(p => p.Watchlists).
           HasForeignKey(x => x.Symbol);
        }




        public DbSet<Stock> Stocks { get; set; }
        public DbSet<MyUser> Users { get; set; }
        public DbSet<Watchlist> Watchlists { get; set; }

    }

}
