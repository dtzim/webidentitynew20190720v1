﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StackForceApi.Models;
using WebAPI_Identity.Data;

namespace StackForceApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StocksController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public StocksController(ApplicationDbContext context)
        {
            _context = context;

            //if (!_context.Stocks.Any())
            //{
            //    _context.Stocks.Add(new Stock
            //    {
            //        Symbol = "Symbol",
            //        Description = "Description",
            //        Currency = "Currency",
            //        StockExchange = "StockExchangeLong"

            //    });
            //    _context.Stocks.Add(new Stock
            //    {
            //        Symbol = "Symbol2",
            //        Description = "Description2",
            //        Currency = "Currency2",
            //        StockExchange = "StockExchangeLong2"

            //    });
            //    _context.SaveChanges();
                
            //}

        }

        // GET: api/Stocks
        //[AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Stock>>> GetStocks()
        {
            return await _context.Stocks.ToListAsync();
        }

        // GET: api/Stocks/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Stock>> GetStock(string id)
        {
            var stock = await _context.Stocks.FindAsync(id);

            if (stock == null)
            {
                return NotFound();
            }

            return stock;
        }

        // PUT: api/Stocks/5
       // [HttpPut("{id}")]
      //  public async Task<IActionResult> PutStock(string id, Stock stock)
      //  {
      //      if (id != stock.Symbol)
      //      {
       //         return BadRequest();
      //      }

      //      _context.Entry(stock).State = EntityState.Modified;

       //     try
       //     {
       //         await _context.SaveChangesAsync();
       //     }
      //      catch (DbUpdateConcurrencyException)
       //     {
         //       if (!StockExists(id))
        //        {
        //            return NotFound();
        //        }
        //        else
         //       {
         //           throw;
         //       }
        //    }

        //    return NoContent();
       // }

        // POST: api/Stocks
       // [HttpPost]
      //  public async Task<ActionResult<Stock>> PostStock(Stock stock)
       // {
       //     _context.Stocks.Add(stock);
       //     await _context.SaveChangesAsync();

       //     return CreatedAtAction("GetStock", new { id = stock.Id }, stock);
      //  }

        // DELETE: api/Stocks/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Stock>> DeleteStock(int id)
        {
            var stock = await _context.Stocks.FindAsync(id);
            if (stock == null)
            {
                return NotFound();
            }

            _context.Stocks.Remove(stock);
            await _context.SaveChangesAsync();

            return stock;
        }

        private bool StockExists(string id)
        {
            return _context.Stocks.Any(e => e.Symbol == id);
        }
    }
}
