﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using StackForceApi.Models;
using WebAPI_Identity.Data;
using Newtonsoft.Json;
using System.Web.Helpers;
using Microsoft.AspNetCore.Authorization;
using WebAPI_Identity.Services;

namespace StackForceApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class WatchlistsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private IUserService _userService;

        public WatchlistsController(ApplicationDbContext context, IUserService userService)
        {
            _context = context;

            //if (!_context.Watchlists.Any())
            //{
            //    _context.Watchlists.Add(new Watchlist
            //    {
            //        UserId = "UserId1",
            //        Symbol = "Symbol1"

            //    });
            //    _context.Watchlists.Add(new Watchlist
            //    {
            //        UserId = "UserId1",
            //        Symbol = "Symbol2"

            //    });
            //    _context.SaveChanges();
                
            //}

        }

        // GET: api/Watchlists
        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Watchlist>>> GetWatchlists()
        {
            var user = HttpContext.User;
            var userId = "";
            var userObject = new MyUser();

            if (user!=null)
            {
                userId = user.Identity.Name;
                //userObject = _userService.GetById(userId);
            }

            //string userId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var watchlist = _context.Watchlists.Include(s => s.Symbol).Where(i => i.UserId == userId).Select(x => x.Symbol).ToList();
            return Ok(watchlist);
           // return await _context.Watchlists.ToListAsync();
        }

        // GET: api/Watchlists/5
        [HttpGet("getByUserId/{id}")]
        public async Task<ActionResult<IEnumerable<Watchlist>>> GetWatchlist(string id)
        {
            //  var watchlist = await _context.Watchlists.FindAsync(id);

            var list= await  _context.Watchlists.ToListAsync();

            if (list == null)
                {
                  return NotFound();
              }

            var newList = new List<Watchlist>();
            for (int i=0;i<list.Count;i++)
            {
                if(list[i].UserId.Equals(id))
                {
                    list[i].MyUser = null;
                    newList.Add(list[i]);
                }

            }
            return newList;
        }
        

        // PUT: api/Watchlists/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutWatchlist(string id, Watchlist watchlist)
        {
            if (id != watchlist.UserId)
            {
                return BadRequest();
            }

            _context.Entry(watchlist).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!WatchlistExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Watchlists
        [HttpPost]
        public async Task<ActionResult<Watchlist>> PostWatchlist(Watchlist watchlist)
        {
           // string id = watchlist.UserId;
            //  MyUser user= await _context.Users.FindAsync(id);
            //  if (user.StocksWatching.Count()==5) {
            //      return NotFound();
            //   }
            //    else {
            _context.Watchlists.Add(watchlist);
            //}

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (WatchlistExists(watchlist.UserId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetWatchlist", new { id = watchlist.UserId }, watchlist);
        }

        // DELETE: api/Watchlists/5
        [HttpDelete("{id}/{id2}")]
        public async Task<ActionResult<Watchlist>> DeleteWatchlist(string id,string id2)
        {
            var watchlist = await _context.Watchlists.FindAsync(id, id2);
            if (watchlist == null)
            {
                return NotFound();
            }

            _context.Watchlists.Remove(watchlist);
            await _context.SaveChangesAsync();

            return watchlist;
        }

        private bool WatchlistExists(string id)
        {
            return _context.Watchlists.Any(e => e.UserId == id);
        }
    }
}
