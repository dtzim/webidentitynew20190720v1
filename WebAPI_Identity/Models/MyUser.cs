﻿
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
 using StackForceApi.Models;


namespace StackForceApi.Models
{
    public class MyUser : IdentityUser
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }


        public IEnumerable<Watchlist> StocksWatching { get; set; } = new List<Watchlist>();
    }
}
