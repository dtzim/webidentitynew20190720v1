﻿using StackForceApi.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;


namespace StackForceApi.Models
{
    public class Stock

    {
        [Key]
        
        public string Symbol { get; set; }

        public string Description { get; set; }

        public string Currency { get; set; }

        public string StockExchange { get; set; }

        [NotMapped]
        public IEnumerable<Watchlist> Watchlists { get; set; } = new List<Watchlist>();

        
    }
}
