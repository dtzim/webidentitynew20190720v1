﻿using Microsoft.AspNetCore.DataProtection.KeyManagement;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using StackForceApi.Models;

namespace StackForceApi.Models
{
    public class Watchlist
    {

        public string UserId { get; set; }
        public string Symbol { get; set; }
        [NotMapped]
        public MyUser MyUser { get; set; }
        [NotMapped]
        public Stock Stock { get; set; }

    }
}
